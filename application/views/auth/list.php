<!---
 * User: adil
 * Date: 17/11/15
 */
--->
<!DOCTYPE html>
<html>
<head>
    <title>Site</title>
    <?php $this->load->view('base/head'); ?>
</head>
<body>
<div class="uk-grid">
    <div class="uk-width-large-6-10 uk-grid" style="margin: 0 20% 0 20%;">
        <div class="uk-width-3-10 uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-3-10">
            <?php $this->load->view('base/menu'); ?>
        </div>
        <div class="uk-width-7-10 uk-width-small-1-1 uk-width-medium-7-10 uk-width-large-7-10">
            <div class="uk-panel">
                <table class="uk-table">
                    <thead>
                    <tr>
                        <th>Kullanıcı Adı</th>
                        <th>Kullanıcı Mail</th>
                        <th>Kullanıcı Yetki</th>
                        <th>İşlem</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($users as $row): ?>
                    <tr>
                        <td><?=$row->user_name?></td>
                        <td><?=$row->user_mail?></td>
                        <?php
                            if($row->user_role == 1){
                        ?>
                        <td>Yönetici</td>
                                <?php }
                        elseif($row->user_role == 2){
                            ?>
                        <td>Personel Yöneticisi</td>
                            <?php
                        }
                        else{
                        ?><td>Personel</td>
                        <?php } ?>
                        <td>
                            <a href="<?php echo base_url('auth/edit'); ?>?id=<?=$row->user_id?>">Düzenle</a> /
                            <a href="<?php echo base_url('auth/delete'); ?>?id=<?=$row->user_id?>">Sil</a>


                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>



            </div>
        </div>

    </div>
</div>

</body>
</html>