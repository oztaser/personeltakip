<!DOCTYPE html>
<html>
<head>
    <title>Personel Kayıt</title>
    <?php $this->load->view('base/head'); ?>
</head>
<body>
<div class="uk-grid uk-width-medium-1-2 uk-container-center">
    <div class="uk-width-1-1">
    <form action="<?php echo base_url('auth/register'); ?>" method="post" class="uk-form uk-form-horizontal">
        <fieldset>
            <legend>Personel Kaydı</legend>
            <div class="uk-form-row">
                <label class="uk-form-label" for="">Kullancı Adı:</label>
                <input name="user_name" placeholder="Kullanıcı Adı" class="uk-form-width-large" type="text">
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="">Kullanıcı Porola:</label>
                <input name="user_password" placeholder="Kullanıcı Şifresi" class="uk-form-width-large" type="password">
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="">Kullanıcı E-posta</label>
                <input name="user_mail" placeholder="Kullanıcı E-postası" class="uk-form-width-large" type="text">
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="">Gizli Anahatar</label>
                <input name="secret_key" placeholder="Kullanıcı Anahtar" class="uk-form-width-large" type="text">
            </div>
            <!--- 4DDD7DC58FEDD5553AA8D854245C2 --->

            <div class="uk-form-row">
                <button class="uk-button uk-button-primary" type="submit">Kayıt Ol</button>
            </div>
        </fieldset>
    </form>
        <?php
            if(isset($error) == true)
            {
                ?>
                <div class="uk-alert uk-alert-danger">Giriş Yapılamadı..</div>
        <?php
            }
        ?>

    </div>
</div>
</body>
</html>