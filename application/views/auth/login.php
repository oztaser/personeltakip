<!DOCTYPE html>
<html>
<head>
    <title>Personel Takip Login</title>
    <?php $this->load->view('base/head'); ?>
</head>
<body>
<div class="uk-grid uk-width-medium-1-2 uk-container-center login">
    <div class="uk-width-1-1 ">
        <form class="uk-form" action="<?php echo base_url('auth/login'); ?>" method="post" >
            <fieldset data-uk-margin>
                <legend>Kullanıcı Girişi</legend>
                <input type="text" name="user_name" placeholder="Kullanıcı Adı">
                <input type="password" name="user_password" placeholder="Parola">
                <input type="submit" class="uk-button" value="Giriş Yap">
            </fieldset>
        </form>
        <h5 class="uk-text-warning">Gizli anahtar ile personel kaydı yapmak için <a href="<?php echo base_url('auth/register'); ?>">Tıklayınız.</a></h5>
        <?php
            if(isset($error) == true)
            {
                ?>
                <div class="uk-alert uk-alert-danger">Giriş Yapılamadı..</div>
        <?php
            }
        ?>

    </div>
</div>
</body>
</html>
