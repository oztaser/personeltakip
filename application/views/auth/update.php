<!---
 * User: adil
 * Date: 17/11/15
 */
--->
<!DOCTYPE html>
<html>
<head>
    <title>Site</title>
    <?php $this->load->view('base/head'); ?>
</head>
<body>
<div class="uk-grid">
    <div class="uk-width-large-6-10 uk-grid" style="margin: 0 20% 0 20%;">
        <div class="uk-width-3-10 uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-3-10">
            <?php $this->load->view('base/menu'); ?>
        </div>
        <div class="uk-width-7-10 uk-width-small-1-1 uk-width-medium-7-10 uk-width-large-7-10">
            <div class="uk-panel">
                <form action="<?php echo base_url('auth/update'); ?>" method="post" class="uk-form uk-form-horizontal">
                    <fieldset>
                        <legend>Kullanıcı Ekle</legend>
                        <input type="hidden" name="user_id" value="<?php echo $users->user_id; ?>" />
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="">Kullancı Adı:</label>
                            <input type="text" name="user_name" placeholder="Kullanıcı Adı" value="<?php echo $users->user_name; ?>" class="uk-form-width-large" />
                        </div>
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="">Kullanıcı E-posta</label>
                            <input type="text" name="user_mail" placeholder="Kullanıcı E-postası" value="<?php echo $users->user_mail; ?>" class="uk-form-width-large" />
                        </div>
                        <?php if($this->session->userdata('user_role') != 3): ?>
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="">Kullanıcı Yetkisi</label>
                            <select name="user_role" class="uk-form-width-large">
                                <?php
                                    if($users->user_role == 1):
                                ?>
                                <option value="1" selected>Yönetici</option>
                                <option value="2">Personel Yöneticisi</option>
                                <option value="3">Personel</option>
                                <?php endif; if($users->user_role == 2): ?>
                                <option value="1">Yönetici</option>
                                <option value="2" selected>Personel Yöneticisi</option>
                                <option value="3">Personel</option>
                                <?php endif; if($users->user_role == 3): ?>
                                <option value="1">Yönetici</option>
                                <option value="2">Personel Yöneticisi</option>
                                <option value="3" selected>Personel</option>
                                <?php endif; ?>
                            </select>
                        </div>
                        <?php endif ?>
                        <div class="uk-form-row">
                            <button class="uk-button uk-button-primary" type="submit">Kullanıcı Ekle</button>
                        </div>
                    </fieldset>

                </form>
            </div>
        </div>

    </div>
</div>

</body>
</html>