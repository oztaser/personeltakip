<!DOCTYPE html>
<html>
<head>
    <title>Site</title>
    <?php $this->load->view('base/head'); ?>
</head>
<body>
<div class="uk-grid">
    <div class="uk-width-large-6-10 uk-grid" style="margin: 0 20% 0 20%;">
        <div class="uk-width-3-10 uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-3-10">
            <?php $this->load->view('base/menu'); ?>
        </div>
        <div class="uk-width-7-10 uk-width-small-1-1 uk-width-medium-7-10 uk-width-large-7-10">
            <div class="uk-panel">
                <form action="<?php echo base_url('department/update'); ?>" method="post" class="uk-form uk-form-horizontal">
                    <fieldset>
                        <legend>Bölüm Güncelle</legend>
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="">Bölüm Adı:</label>
                            <input type="hidden" name="department_id" value="<?php echo $department->department_id; ?>">
                            <input type="text" name="department_name" placeholder="Bölüm Adı" value="<?php echo $department->department_name ?>" class="uk-form-width-large" />
                        </div>
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="">Bölüm Yetkilisi</label>
                            <select name="department_manager" class="uk-form-width-large">
                                <?php
                                    foreach($employee as $row):
                                        if($row->employee_id == $department->department_manager){
                                ?>
                                            <option selected value="<?php echo $row->employee_id; ?>"><?php echo $row->employee_name ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $row->employee_id; ?>"><?php echo $row->employee_name ?></option>
                                <?php } endforeach; ?>
                            </select>
                        </div>
                        <div class="uk-form-row">
                            <button class="uk-button uk-button-primary" type="submit">Bölüm Güncelle</button>
                        </div>
                    </fieldset>
                    <?php
                    if(isset($error) == true)
                    {
                        ?>
                        <div class="uk-alert uk-alert-danger">Tüm bilgileri eksiksiz doldurun..</div>
                        <?php
                    }
                    ?>
                </form>

            </div>
        </div>

    </div>
</div>

</body>
</html>