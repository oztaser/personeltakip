<div class="uk-panel uk-panel-box">
    <h6 class="uk-panel-title">Hoş geldin <?php echo $this->session->userdata('user_name')?>! <a href="<?php echo base_url('auth/logout'); ?>">Çıkış Yap</a></h6>
    <ul class="uk-nav uk-nav-parent-icon" data-uk-nav>
        <?php if($this->session->userdata('user_role') == 3): ?>
            <li class="uk-parent">
                <a href="#">Düzenlemeler</a>
                <ul class="uk-nav-sub">
                    <li><a href="<?php echo base_url('auth/edit?id='.$this->session->userdata('user_id')); ?>">Giriş Ayarları</a></li>
                    <li><a href="<?php echo base_url('department/add'); ?>">Bölüm Ekle</a></li>
                </ul>
            </li>
        <?php

             endif;
             if($this->session->userdata('user_role') == 2):
        ?>
        <li class="uk-parent">
            <a href="#">Bölümler</a>
            <ul class="uk-nav-sub">
                <li><a href="<?php echo base_url('department/index'); ?>">Bölüm Listesi</a></li>
                <li><a href="<?php echo base_url('department/add'); ?>">Bölüm Ekle</a></li>
            </ul>
        </li>

        <?php
             endif;
             if($this->session->userdata('user_role') == 1):
        ?>

        <li class="uk-parent">
            <a href="#">Bölümler</a>
            <ul class="uk-nav-sub">
                <li><a href="<?php echo base_url('department/index'); ?>">Bölüm Listesi</a></li>
                <li><a href="<?php echo base_url('department/add'); ?>">Bölüm Ekle</a></li>
            </ul>
        </li>
        <li class="uk-parent">
            <a href="#">Kullanıcılar</a>
            <ul class="uk-nav-sub">
                <li><a href="<?php echo base_url('auth/user_list'); ?>"> Kullanıcı Listesi</a></li>
                <li><a href="<?php echo base_url('auth/add'); ?>">Yeni Kullanıcı Ekle</a></li>
            </ul>
        </li>
        <?php endif; ?>
    </ul>
</div>