<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends Login_Controller {

    function __construct()
    {
        parent:: __construct();
    }

    function index()
    {
        $this->load->view('site/index');
    }
}
