<?php

class Department extends Authority_Controller
{
    function __construct()
    {
        parent:: __construct();
        $this->load->model('department_model', '', TRUE);
        $this->load->model('employee_model', '' , TRUE);
    }

    /***
     * departman index sayfası.
     * tablo olarak departman listesinin çıktı verir.
     */
    function index()
    {
        $data['departments'] = $this->department_model->department_list();
        $this->load->view('department/index', $data);
    }
}
