<?php
/**
 * Created by PhpStorm.
 * User: adil
 * Date: 15/11/15
 * Time: 16:11
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends Authority_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', '', TRUE);
    }

    function index()
    {
        if($this->session->userdata('is_logged_in'))
            redirect(base_url('site/index'));
        else
            redirect(base_url('auth/login'));
    }

    /**
     * Kullanıcı girişi var mı diye kontrol edip varsa site index'e yönlendirir.
     */
    function is_logged()
    {
        if($this->session->userdata('is_logged_in') == null)
            redirect(base_url('auth/login'));

    }

    function login()
    {
        $data_error['error'] = true;
        if($this->input->post() == false){
            $this->load->view('auth/login');
        }
        else{
            $data = array(
                'user_name'=> $this->input->post('user_name'),
                'user_password'=> md5($this->input->post('user_password'))
            );

            if($this->checkData($data) == true)
                $this->load->view('auth/login', $data_error);
            else{
                if($this->user_model->login_user($data) == true)
                {
                    $user_data = $this->user_model->get_user_by_name($data['user_name'], $data['user_password']);
                    $session_data = array();
                    foreach($user_data as $key => $value){
                        $session_data[$key] = $value;
                    }
                    $session_data['is_logged_in'] = true;

                    $this->session->set_userdata($session_data);
                    redirect(base_url('site/index'));
                }
                else{
                    $this->load->view('auth/login', $data_error);
                }
            }
        }
    }

    function user_list()
    {
        $this->is_logged();
        $data['users'] = $this->user_model->user_list();
        $this->load->view('auth/list', $data);
    }

    function register()
    {
        $data_error['error'] = true;
        if($this->input->post() == null)
            $this->load->view('auth/register');
        else{
            $data = array(
                'user_name'=> $this->input->post('user_name'),
                'user_password'=> md5($this->input->post('user_password')),
                'user_mail'=> $this->input->post('user_mail'),
                'user_role'=> 3,
            );
            if($this->checkData($data) == true && $this->input->post('secret_key') != '4DDD7DC58FEDD5553AA8D854245C2')
                $this->load->view('auth/register', $data_error);
            else {
                $this->user_model->userAdd($data);
                redirect(base_url('auth/login'));
            }
        }
    }

    /**
     * @param $data
     * @return bool
     * Post edilen dataların boş olup olmadığını kontrol eder.
     */
    function checkData($data)
    {
        $error = false;
        foreach($data as $q) {
            if ($q == null) {
                $error = true;
                break;
            }
        }

        return $error;
    }

    /**
     * Post edilen dataları kontrol eder.
     * Bir sıkıntı yoksa user_model ile kullanıcıyı ekler ve kullanıcı listesine yönlenir.
     */
    function add()
    {
        $this->is_logged();
        $data_error['error'] = true;
        if($this->input->post() == false)
            $this->load->view('auth/add');
        else{
            $data = array(
                'user_name'=> $this->input->post('user_name'),
                'user_password'=> md5($this->input->post('user_password')),
                'user_mail'=> $this->input->post('user_mail'),
                'user_role'=> $this->input->post('user_role')
            );
            if($this->checkData($data) == true)
                $this->load->view('auth/add', $data_error);
            else {
                $this->user_model->userAdd($data);
                redirect(base_url('auth/user_list'));
            }
        }
    }

    function logout()
    {
        $this->is_logged();
        $this->session->sess_destroy();
        redirect(base_url('auth/login'));
    }

    function update()
    {
        $this->is_logged();
        $data_error['error'] = true;
        if($this->input->post() == false)
            redirect(base_url('auth/user_list'));
        else{
            if($this->input->post('user_role') != null)
            {
                $data = array(
                'user_id'=> $this->input->post('user_id'),
                'user_name'=> $this->input->post('user_name'),
                'user_mail'=> $this->input->post('user_mail'),
                'user_role'=> $this->input->post('user_role')
                );
            }else{
                $data = array(
                    'user_id'=> $this->input->post('user_id'),
                    'user_name'=> $this->input->post('user_name'),
                    'user_mail'=> $this->input->post('user_mail')
                );
            }
            if($this->checkData($data) == true)
            {
                echo "a";
            }
            else{
                $this->user_model->userUpdate($data);
                redirect(base_url('auth/user_list'));
            }
        }
    }

    function edit()
    {
        $this->is_logged();
        $data_error['error'] = true;
        if($this->input->get() == false)
            redirect(base_url('auth/user_list'));
        else{
            $user_id = $this->input->get('id');
            if($user_id == null)
                redirect(base_url('auth/user_list'));
            else{
                $user_data['users'] = $this->user_model->get_user($user_id);
                $this->load->view('auth/update', $user_data);
            }
        }
    }

    /**
     * get ile gelen kullanıcı idsini user_model altındaki userDelete fonksiyonu ile siler.
     */
    function delete()
    {
        $this->is_logged();
        if($this->input->get() == false)
            redirect(base_url('auth/user_list'));
        else{
            $data = array(
                'user_id'=> $this->input->get('id'),
            );
            if($this->checkData($data) == true)
                redirect(base_url('auth/user_list'));
            else{
                $this->user_model->userDelete($data);
                redirect(base_url('auth/user_list'));
            }
        }
    }
}