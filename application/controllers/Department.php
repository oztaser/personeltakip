<?php

class Department extends Authority_Controller
{
    function __construct()
    {
        parent:: __construct();
        $this->load->model('department_model', '', TRUE);
        $this->load->model('employee_model', '' , TRUE);
    }

    /***
     * departman index sayfası.
     * tablo olarak departman listesinin çıktı verir.
     */
    function index()
    {
        $data['departments'] = $this->department_model->department_list();
        $this->load->view('department/index', $data);
    }

    /**
     * @param $data
     * @return bool
     * Post edilen dataların boş olup olmadığını kontrol eder.
     */
    function checkData($data)
    {
        $error = false;
        foreach($data as $q) {
            if ($q == null) {
                $error = true;
                break;
            }
        }

        return $error;
    }

    /**
     * sayfa post edilmemiş ise boş kayıt formu döndürür
     * eğer sayfa post edilmiş ise post edilen bilgilerle yeni departman kaydı oluşturur.
     */
    function add()
    {
        if($this->input->post() == false) {
            $employee_list['employee'] = $this->employee_model->employee_list();
            $this->load->view('department/add', $employee_list);
        }
        else{
            $data =array(
                'department_name'=> $this->input->post('department_name'),
                'department_manager'=> $this->input->post('department_manager')
            );
            if($this->checkData($data) == true){
                $employee_list['employee'] = $this->employee_model->employee_list();
                $employee_list['error'] = true;
                $this->load->view('department/add', $employee_list);
            }
            else{
                $this->department_model->departmentAdd($data);
                redirect(base_url('department/index'));
            }
        }
    }

    /**
     * department/update sayfasında post edilen bilgiler kontrol eder
     * department_modeldaki departmenUpdate metodu ile veri güncellemesi yapar
     */
    function update()
    {
        if($this->input->post() == false)
            redirect(base_url('department/index'));
        else{
            $data = array(
                'department_id'=> $this->input->post('department_id'),
                'department_name'=> $this->input->post('department_name'),
                'department_manager'=> $this->input->post('department_manager')
            );
            print_r($data);
            if($this->checkData($data) == false)
            {
                $this->department_model->department_model->departmentUpdate($data);
                redirect(base_url('department/index'));
            }

        }
    }

    /***
     * get metodu ile gelen bir id değeri varmı diye kontrol edip eğer varsa bu değere ait bilgileri update view'ine yollar
     * gelen bir id değeri yoksa department/index sayfasına gider
     */
    function edit()
    {
        if($this->input->get() == false)
            redirect(base_url('department/index'));
        else{
            $department_id = $this->input->get('id');
            if($department_id == null)
                redirect(base_url('department/index'));
            else{
                $department_data['department'] = $this->department_model->get_department($department_id);
                $department_data['employee'] = $this->employee_model->employee_list();
                $this->load->view('department/update', $department_data);
            }
        }
    }

    /**
     * get ile gelen departan idsini department_model altındaki departmentDelete fonksiyonu ile siler.
     */
    function delete()
    {
        if($this->input->get() == false)
            redirect(base_url('department/index'));
        else{
            $data = array(
                'department_id'=> $this->input->get('id')
            );
            if($this->checkData($data) == true)
                redirect(base_url('department/index'));
            else{
                $this->department_model->departmentDelete($data);
                redirect(base_url('department/index'));
            }
        }
    }
}