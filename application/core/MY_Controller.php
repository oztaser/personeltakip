<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public function __construct ()
    {
        parent::__construct();
    }
}

class Login_Controller extends MY_Controller
{
    public function __construct ()
    {
        parent::__construct();
        if($this->router->fetch_class() != 'auth')
          $this->is_logged();
    }
    /**
     * Kullanıcı girişi var mı diye kontrol edip varsa site index'e yönlendirir.
     */
    function is_logged()
    {
        if($this->session->userdata('is_logged_in') == null)
            redirect('auth/login');
    }
}

class Authority_Controller extends Login_Controller
{
    public function __construct()
    {
        parent::__construct();

        $user_role = $this->session->userdata('user_role');
        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        if($user_role == 3 && ($controller == 'department' || $method == 'user_list' || $method == 'add' || $method == 'delete'))
            redirect(base_url('site'));
    }
}