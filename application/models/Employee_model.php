<?php
/**
 * User: adil
 * Date: 21/11/15
 */
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Employee_model extends CI_Model
{
    function  __construct()
    {
        parent:: __construct();
    }

    /**
     * @return mixed
     * emplooye_id azalan şekilde tüm employee listesini verir.
     */
    function employee_list()
    {
        $query = $this->db->order_by("employee_id", "desc");
        $query = $this->db->get('employee');
        return $query->result();
    }

}