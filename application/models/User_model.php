<?php
/**
 * User: adil
 * Date: 17/11/15
 */
if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    function  __construct()
    {
        parent:: __construct();
    }

    function get_user($id)
    {
        $this->db->where('user_id', $id);
        $query = $this->db->get('user');

        return $query->row();
    }

    function get_user_by_name($user_name,$user_password)
    {
        $this->db->where('user_name', $user_name);
        $this->db->where('user_password', $user_password);
        $query = $this->db->get('user');

        return $query->row();
    }

    /**
     * @return mixed
     * user_id azalan şekilde tüm kullanıcıların listesini verir.
     */
    function user_list()
    {
        $query = $this->db->order_by("user_id", "desc");
        $query = $this->db->get('user');
        return $query->result();
    }
    /**
     * @param $data
     * @return bool
     * Kontrol edilmiş gelen datalar ile kullanıcı kontrolü yapılıp geriye bool değer dönürür.
     */
    function login_user($data)
    {
        $state = false;
        $this->db->where('user_name', $data['user_name']);
        $this->db->where('user_password', $data['user_password']);
        $q = $this->db->get('user');
        if ($q->result_array())
            $state = true;

        return $state;
    }

    /**
     * @param $data
     * @return mixed
     * Gelen datalar ile kullanıcı ekler.
     */
    function userAdd($data)
    {
        return $this->db->insert('user', $data);
    }

    function userUpdate($data)
    {
        $this->db->where('user_id', $data['user_id']);
        $this->db->update('user', $data);
    }

    /**
     * @param $data
     * @return mixed
     * $data ile gelen idye ait kullanıcıyı siler.
     */
    function userDelete($data)
    {
        return $this->db->delete('user', $data);
    }

}