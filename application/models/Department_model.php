<?php
/**
 * User: adil
 * Date: 17/11/15
 */
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Department_model extends CI_Model
{
    function  __construct()
    {
        parent:: __construct();
    }

    /**
     * @return mixed
     * department_id azalan şekilde tüm departmanların listesini verir.
     */
    function department_list()
    {
        $this->db->select('department_id, department_name, employee_name');
        $this->db->from('department');
        $this->db->join('employee', 'employee.employee_id = department.department_manager');
        $query = $this->db->order_by("department_id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    /***
     * @param $id
     * @return mixed
     * aldığı id değerine ait department'ın tüm bilgilerini return eder
     */
    function get_department($id)
    {
        $this->db->where('department_id', $id);
        $query = $this->db->get('department');

        return $query->row();
    }

    /**
     * @param $data
     * @return mixed
     * Gelen datalar ile yeni bir departman ekler.
     */
    function departmentAdd($data)
    {
        return $this->db->insert('department', $data);
    }

    /***
     * @param $data
     * $data dizisi ile aldığı verileri kullanarak department güncellemesi yapar
     */
    function departmentUpdate($data)
    {
        $this->db->where('department_id', $data['department_id']);
        $this->db->update('department', $data);
    }

    /**
     * @param $data
     * @return mixed
     * $data ile gelen idye ait departmanı siler.
     */
    function departmentDelete($data)
    {
        return $this->db->delete('department', $data);
    }

}